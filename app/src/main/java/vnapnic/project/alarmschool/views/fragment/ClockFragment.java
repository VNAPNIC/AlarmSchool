package vnapnic.project.alarmschool.views.fragment;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import vnapnic.project.alarmschool.R;
import vnapnic.project.alarmschool.base.fragment.BaseFragments;
import vnapnic.project.alarmschool.common.Logger;
import vnapnic.project.alarmschool.common.media;
import vnapnic.project.alarmschool.controller.fragments.IFragmentClock;
import vnapnic.project.alarmschool.controller.fragments.UpdateableFragment;
import vnapnic.project.alarmschool.model.OverLookModel;
import vnapnic.project.alarmschool.model.SeasonsModel;
import vnapnic.project.alarmschool.model.entities.Alarm;
import vnapnic.project.alarmschool.model.entities.OverLook;
import vnapnic.project.alarmschool.model.entities.RepeatWeekdays;
import vnapnic.project.alarmschool.model.entities.Seasons;
import vnapnic.project.alarmschool.views.dialogs.OverlayService;
import vnapnic.project.alarmschool.views.viewholder.FragmentClockViewHolder;

public class ClockFragment extends BaseFragments implements IFragmentClock, UpdateableFragment {

    private final String TAG = ClockFragment.class.getName();
    private FragmentClockViewHolder holder;
    private List<Seasons> seasonsList;
    private List<OverLook> overLookList;
//    private List<Alarm> alarmList;

    private SeasonsModel seasonsModel;
    private OverLookModel lookModel;
//    private MediaPlayer mediaPlayer;


    public ClockFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
            /* Fill base view */
            root = inflater.inflate(R.layout.fragment_clock, container, false);
            seasonsModel = new SeasonsModel(getContext());
            lookModel = new OverLookModel(getContext());
        } catch (Exception e) {
            e.printStackTrace();
            root = new View(getActivity());
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            holder = new FragmentClockViewHolder(getActivity(), root, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            holder.textClock.addTextChangedListener(new setTextWatcher());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        try {

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected String getTagId() {
        return TAG;
    }

    public class setTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable str) {
            try {
                String[] strs = str.toString().split("\n");
                Logger.d(TAG, strs[0] + " ------------------------");
                Logger.d(TAG, strs[1] + " ------------------------");
                seasonsList = seasonsModel.getAll();
                overLookList = lookModel.getAll();

                String weekDay;
                SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE", Locale.US);

                Calendar calendar = Calendar.getInstance();
                weekDay = dayFormat.format(calendar.getTime());
                Logger.d(TAG, weekDay + " ------------------------ weekDay");
                String converVI = converENtoVI(weekDay);
                Logger.d(TAG, converVI + " ------------------------ converVI");
                for (int i = 0; i < seasonsList.size(); i++) {
                    if (TextUtils.isEmpty(seasonsList.get(i).months)) {
                    } else {
                        String[] months = seasonsList.get(i).months.split(",");
                        if (months.length >= 2) {
                            //check months
                            if (checkSeason(strs[1], months[0].trim(), months[1].trim())) {
                                //check months success.!
                                Logger.d(TAG, "Mua: " + seasonsList.get(i).title);
                                List<Alarm> alarmList = seasonsList.get(i).objs;//cast
                                if (alarmList != null) {
                                    if (alarmList.size() > 0) { // check size alarm
                                        for (int j = 0; j < alarmList.size(); j++) { // get All alarm
                                            Logger.d(TAG,
                                                    alarmList.get(j).alarmTimer.toString().toLowerCase().trim()
                                                            + " --- "
                                                            + strs[0].toString().toLowerCase().trim() + " ^^^^ Result: "
                                                            + alarmList.get(j).alarmTimer.toString().toLowerCase().trim()
                                                            .equals(strs[0].toString().toLowerCase().trim()));

                                            if (alarmList.get(j).alarmTimer.toString().toLowerCase().trim()
                                                    .equals(strs[0].toString().toLowerCase().trim())) {
                                                // Check alarm
                                                Logger.d(TAG, "Timer Success : " + alarmList.get(j).title);

                                                if (checkLook(strs[1].trim())) { // check Overlook
                                                    Logger.d(TAG, "Overlook true : ");
                                                } else { // 1overlook
                                                    Logger.d(TAG, "Overlook false : ");
                                                    List<RepeatWeekdays> weekdayses = alarmList.get(j).objs;
                                                    if (weekdayses != null) {//check null weekday
                                                        if (weekdayses.size() > 0) {// check size weekday
                                                            for (int w = 0; w < weekdayses.size(); w++) {//get ALl weekday
                                                                if (checkRepeat(converVI, weekdayses.get(w).repeatWeekday)) {// check weekday
                                                                    if (alarmList.get(j).isRun == 1) {// check isRun
                                                                        try {
                                                                            Logger.d("Alarm success", "Alarm success " + alarmList.get(j).music);
                                                                            media.run(getActivity().getApplication(), alarmList.get(j).music);
                                                                            launchOverlayService();
//                                                                            SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
//                                                                            Date stop = sdf.parse(strs[1].toLowerCase().toString() + " " + alarmList.get(j).alarmStop);
//                                                                            Logger.d(TAG, stop + " start ----- " + stop + " stop");
//                                                                            Calendar conver = Calendar.getInstance();
//                                                                            conver.setTime(stop);
//                                                                            double timeNow = System.currentTimeMillis();
//                                                                            double coutDown = conver.getTimeInMillis() - timeNow;
                                                                            Logger.d("CountDownTimer", "CountDownTimer: " + media.getDuration());
                                                                            new CountDownTimer(Math.round(media.getDuration()), 1000) {

                                                                                public void onTick(long millisUntilFinished) {
                                                                                    Logger.d(TAG, "CountDownTimer  " + ConvertMilliSecondsToFormattedDate(millisUntilFinished + ""));
                                                                                }

                                                                                public void onFinish() {
                                                                                    try {
                                                                                        media.stop(getActivity());
                                                                                    } catch (Exception e) {
                                                                                        e.printStackTrace();
                                                                                    }
                                                                                }

                                                                            }.start();

                                                                        } catch (Exception e) {
                                                                            e.printStackTrace();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                            } else {
                                                Logger.d("Alarm fail", "Alarm fial");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    ///-------------------------------------------------------------------////

    private void launchOverlayService() {
        try {
            Intent intent = new Intent(getActivity(), OverlayService.class);
            getActivity().startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("onCreate", e.getMessage());
        }
    }

    //-----------------------------------------------------------------------///
    public String dateFormat = "HH:mm:ss.SSS";
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);

    public String ConvertMilliSecondsToFormattedDate(String milliSeconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.parseLong(milliSeconds));
        return simpleDateFormat.format(calendar.getTime());
    }

    private boolean checkRepeat(String weekday, String repeat) {
        try {
            if (!TextUtils.isEmpty(repeat)) {
                Logger.d(TAG, "weekday: " + weekday + " - repeat: " + repeat);
                String[] repeats = repeat.split(",");
                if (repeats.length > 0) {
                    for (int i = 0; i < repeats.length; i++) {
                        Logger.d(TAG, "repeat: " + repeats[i]);
                        if (repeats[i].toString().toLowerCase().trim().equals(weekday.toString().toLowerCase().trim())) {
                            return true;
                        }
                    }
                } else {
                    return false;
                }

            } else {
                return false;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean checkLook(String dateNow) {
        try {
            if (overLookList != null) {
                if (overLookList.size() > 0) {
                    for (int i = 0; i < overLookList.size(); i++) {
                        if (!TextUtils.isEmpty(overLookList.get(i).overlook)) {
                            String[] looks = overLookList.get(i).overlook.split(",");
                            if (looks.length >= 2) {
                                if (checkSeason(dateNow, looks[0], looks[1])) {
                                    return true;
                                }
                            }
                        }
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean checkSeason(String dateNow, String startDate, String stopDate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM");
            Date now = sdf.parse(dateNow);
            Date start = sdf.parse(startDate);
            Date stop = sdf.parse(stopDate);

            Logger.d(TAG, sdf.format(now));
            Logger.d(TAG, sdf.format(start));
            Logger.d(TAG, sdf.format(stop));

            Calendar calNow = Calendar.getInstance();
            Calendar calStart = Calendar.getInstance();
            Calendar calStop = Calendar.getInstance();

            calNow.setTime(now);
            calStart.setTime(start);
            calStop.setTime(stop);

            if (calNow.after(calStart) && calNow.before(calStop) || calNow.equals(calStart) || calNow.equals(calStop)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private String converENtoVI(String weekDay) {
        String t;
        switch (weekDay.toLowerCase().trim()) {
            case "monday":
                t = "t2";
                break;
            case "tuesday":
                t = "t3";
                break;
            case "wednesday":
                t = "t4";
                break;
            case "thursday":
                t = "t5";
                break;
            case "friday":
                t = "t6";
                break;
            case "saturday":
                t = "t7";
                break;
            case "sunday":
                t = "cn";
                break;
            default:
                t = null;
                break;
        }
        return t;
    }

    private String format() {
        long timeStamp = System.currentTimeMillis();
        Date curDate = new Date();
        SimpleDateFormat format = new SimpleDateFormat(FORMAT_DATE);
        curDate.setTime(timeStamp);
        return format.format(curDate);
    }

    @Override
    public void update() throws Exception {
//        holder.setData();
        Logger.d(TAG, "update data.!");
    }
}
