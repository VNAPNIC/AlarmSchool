package vnapnic.project.alarmschool.views.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;

import java.util.Calendar;

import vnapnic.project.alarmschool.R;
import vnapnic.project.alarmschool.controller.IDialogDatePiker;
import vnapnic.project.alarmschool.model.entities.BaseAlarmEntities;
import vnapnic.project.alarmschool.views.adapter.DialogSettingAdapter;
import vnapnic.project.alarmschool.widget.MonthView;

/**
 * Created by hainam1421 on 1/29/2016.
 */
public class DialogDatePicker extends Dialog implements MonthView.OnDateSelectedListener {

    private final String TAG = DialogDatePicker.class.getName();
    private View root;
    private MonthView monthView;
    private IDialogDatePiker datePicker;
    private int type;
    private DialogSettingAdapter.ViewHolder holder;
    private View view;
    private BaseAlarmEntities obj;

    public DialogDatePicker(Context context,IDialogDatePiker datePicker,DialogSettingAdapter.ViewHolder holder,View view,BaseAlarmEntities obj,int type) {
        super(context);
        try {
            this.datePicker = datePicker;
            this.holder = holder;
            this.view = view;
            this.type = type;
            this.obj= obj;
            root = getLayoutInflater().inflate(R.layout.dialog_date_picker, null, false);
            this.setContentView(root);
            monthView = (MonthView) root.findViewById(R.id.datePicker);
            initialize(context);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initialize(Context context) throws Exception {
        monthView.setOnDateSelectedListener = this;
    }

    @Override
    public void onDateSelected(Calendar date) {
        try {
            datePicker.onDatePicker(date,holder,view,obj,type);
            dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        ((EditText) view).setText(format.format(date));
    }
}
