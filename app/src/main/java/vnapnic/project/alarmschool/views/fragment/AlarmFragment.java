package vnapnic.project.alarmschool.views.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import vnapnic.project.alarmschool.R;
import vnapnic.project.alarmschool.base.controller.BaseInterface;
import vnapnic.project.alarmschool.base.fragment.BaseFragments;
import vnapnic.project.alarmschool.common.Logger;
import vnapnic.project.alarmschool.controller.fragments.IFragmentAlarm;
import vnapnic.project.alarmschool.controller.fragments.UpdateableFragment;
import vnapnic.project.alarmschool.views.viewholder.FragmentAlarmViewHolder;

public class AlarmFragment extends BaseFragments implements IFragmentAlarm,UpdateableFragment {

    private final String TAG = AlarmFragment.class.getName();
    private FragmentAlarmViewHolder holder;
    public AlarmFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
            /* Fill base view */
            root = inflater.inflate(R.layout.fragment_alarm, container, false);
        } catch (Exception e) {
            e.printStackTrace();
            root = new View(getActivity());
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            holder = new FragmentAlarmViewHolder(getActivity(),root,this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        try {

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected String getTagId() {
        return TAG;
    }

    @Override
    public void update() throws Exception {
        holder.setData();
        Logger.d(TAG, "update data.!");
    }
}