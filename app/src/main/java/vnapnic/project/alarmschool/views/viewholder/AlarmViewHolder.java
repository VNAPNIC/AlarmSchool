package vnapnic.project.alarmschool.views.viewholder;

import android.support.v7.widget.SwitchCompat;
import android.widget.TextView;

/**
 * Created by hainam1421 on 1/20/2016.
 */
public class AlarmViewHolder {
    public TextView timer;
    public TextView title;
    public TextView weekday;
    public TextView music;
    public SwitchCompat isRun;
}
