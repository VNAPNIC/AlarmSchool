package vnapnic.project.alarmschool.views.viewholder;

import android.support.v7.widget.SwitchCompat;
import android.widget.CheckedTextView;
import android.widget.TextView;

/**
 * Created by hainam1421 on 1/20/2016.
 */
public class SeasonViewHolder {
    public CheckedTextView title;
    public SwitchCompat isRun;
}
