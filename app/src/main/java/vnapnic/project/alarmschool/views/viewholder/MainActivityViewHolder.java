package vnapnic.project.alarmschool.views.viewholder;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import vnapnic.project.alarmschool.R;
import vnapnic.project.alarmschool.base.controller.BaseInterface;
import vnapnic.project.alarmschool.common.Logger;
import vnapnic.project.alarmschool.controller.activities.IMainActivities;
import vnapnic.project.alarmschool.model.entities.Alarm;
import vnapnic.project.alarmschool.model.entities.Setting;
import vnapnic.project.alarmschool.views.adapter.DialogSettingAdapter;
import vnapnic.project.alarmschool.views.adapter.FragmentAdapter;
import vnapnic.project.alarmschool.base.viewholder.BaseViewHolder;
import vnapnic.project.alarmschool.views.adapter.SettingAdapter;
import vnapnic.project.alarmschool.views.fragment.AlarmFragment;
import vnapnic.project.alarmschool.views.fragment.ClockFragment;

/**
 * Created by hainam1421 on 1/15/2016.
 */
public class MainActivityViewHolder extends BaseViewHolder implements IMainActivities {

    private final String TAG = MainActivityViewHolder.class.getName();
    public DrawerLayout drawerLayout;
    public Toolbar toolbar;
    public ActionBarDrawerToggle drawerToggle;
    public ActionBar actionBar;
    public ViewPager pager;
    private FragmentAdapter pageAdapter;
    private TabLayout tabLayout;
    private SettingAdapter settingAdapter;
    private ListView listSetting;
    private IMainActivities iMainActivities;
    public Button button;

    public MainActivityViewHolder(Context context, View root, IMainActivities iMainActivities) throws Exception {
        super(context, root, iMainActivities);
        this.iMainActivities = iMainActivities;
        tabLayout = (TabLayout) root.findViewById(R.id.tabanim_tabs);
        drawerLayout = (DrawerLayout) root.findViewById(R.id.drawerLayout);
        toolbar = (Toolbar) root.findViewById(R.id.toolbar);
        pager = (ViewPager) root.findViewById(R.id.pager);
        listSetting = (ListView) root.findViewById(R.id.listSetting);

        AppCompatActivity activity = ((AppCompatActivity) context);
        activity.setSupportActionBar(toolbar);

        actionBar = activity.getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDefaultDisplayHomeAsUpEnabled(false);
        button = (Button) root.findViewById(R.id.confix);
        initialize(context);
    }

    @Override
    protected void initialize(Context context) throws Exception {
        pageAdapter = new FragmentAdapter(((AppCompatActivity) context).getSupportFragmentManager());
        pager.setAdapter(pageAdapter);
        tabLayout.setupWithViewPager(pager);

        settingAdapter = new SettingAdapter(context, this);
        listSetting.setAdapter(settingAdapter);
        setDataSetting();
        setViewPage();
    }

    public void setViewPage() throws Exception{
        pageAdapter.setData();
        pager.setOffscreenPageLimit(pageAdapter.getCount());
        tabLayout.setupWithViewPager(pager);
        tabLayout.getTabAt(0).setIcon(R.mipmap.ic_clock);
        tabLayout.getTabAt(1).setIcon(R.mipmap.ic_alarm);
    }

    public void setDataSetting() throws Exception {
        String[] data = new String[]{
                root.getResources().getString(R.string.seasons_setting),
                root.getResources().getString(R.string.seasons_overlook),
                root.getResources().getString(R.string.setting_addnew)
        };
        int[] icon = new int[]{
                R.mipmap.ic_setting,
                R.mipmap.ic_setting,
                android.R.drawable.ic_menu_add
        };
        List<Setting> oldData = settingAdapter.getData();
        if (oldData == null) {
            oldData = new ArrayList<>();
        } else {
            oldData.clear();
        }
        List<Setting> newData = new ArrayList<>();
        for (int i = 0; i < data.length; i++) {
            Setting item = new Setting();
            item.title = data[i];
            item.icon = icon[i];
            newData.add(item);
        }
        oldData.addAll(newData);
        settingAdapter.notifyDataSetChanged();
    }

//    public void update() throws Exception {
//        Logger.d(TAG, "update data.!");
//        pageAdapter.getItemPosition(new ClockFragment());
//        pageAdapter.getItemPosition(new AlarmFragment());
//    }

    @Override
    public void onPickMusic(Alarm alarm, DialogSettingAdapter adapter) throws Exception {
        iMainActivities.onPickMusic(alarm, adapter);
    }
}