package vnapnic.project.alarmschool.views.adapter;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import vnapnic.project.alarmschool.R;
import vnapnic.project.alarmschool.common.Instance;
import vnapnic.project.alarmschool.common.Logger;
import vnapnic.project.alarmschool.controller.IDialogDatePiker;
import vnapnic.project.alarmschool.controller.IDialogTimePiker;
import vnapnic.project.alarmschool.controller.fragments.IDialogSetting;
import vnapnic.project.alarmschool.model.AlarmModel;
import vnapnic.project.alarmschool.model.OverLookModel;
import vnapnic.project.alarmschool.model.SeasonsModel;
import vnapnic.project.alarmschool.model.entities.Alarm;
import vnapnic.project.alarmschool.model.entities.BaseAlarmEntities;
import vnapnic.project.alarmschool.model.entities.OverLook;
import vnapnic.project.alarmschool.model.entities.RepeatWeekdays;
import vnapnic.project.alarmschool.model.entities.Seasons;
import vnapnic.project.alarmschool.views.dialogs.DialogDatePicker;
import vnapnic.project.alarmschool.views.dialogs.DialogTimePiker;

/**
 * Created by hainam1421 on 1/28/2016.
 */
public class DialogSettingAdapter extends RecyclerView.Adapter<DialogSettingAdapter.ViewHolder>
        implements Instance, IDialogDatePiker, IDialogTimePiker, View.OnClickListener {

    private final String TAG = DialogSettingAdapter.class.getName();
    private Context context;
    private LayoutInflater inflater;
    private List<BaseAlarmEntities> data;
    private IDialogSetting iDialogSetting;

    public DialogSettingAdapter(Context context, IDialogSetting iDialogSetting) {
        data = new ArrayList<>();
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.iDialogSetting = iDialogSetting;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = new View(parent.getContext());
        switch (viewType) {
            case TYPE_SETTING_SEASON:
                view = inflater.inflate(R.layout.setting_season, parent, false);
                break;
            case TYPE_OVERLOOK:
                view = inflater.inflate(R.layout.setting_overlook, parent, false);
                break;
            case TYPE_ADD:
                view = inflater.inflate(R.layout.setting_alarm, parent, false);
                break;
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        try {
            if (data != null) {
                final int viewType = getItemViewType(position);
                final BaseAlarmEntities entities = data.get(position);
                setViewDefaul(holder);
                switch (viewType) {
                    case TYPE_SETTING_SEASON:
                        Seasons seasons = (Seasons) entities;
                        if (!TextUtils.isEmpty(seasons.title)) {
                            holder.title.setText(seasons.title);
                        } else {

                        }
                        if (!TextUtils.isEmpty(seasons.months)) {
                            String[] modths = seasons.months.trim().split(",");
                            if (modths.length >= 2) {
                                Logger.d(TAG, modths[0] + " -> " + modths[1]);
//                                holder.dateStart.setText(modths[0]);
//                                holder.dateStop.setText(modths[1]);

                                String dateFormat = "dd-MM";
                                SimpleDateFormat sourceDateFormat = new SimpleDateFormat("dd-MM");

                                Date date1 = sourceDateFormat.parse(modths[0].toString());
                                SimpleDateFormat targetDateFormat = new SimpleDateFormat(dateFormat);
                                holder.dateStart.setText(targetDateFormat.format(date1));

                                Date date2 = sourceDateFormat.parse(modths[1].toString());
                                SimpleDateFormat targetDateFormat2 = new SimpleDateFormat(dateFormat);

                                holder.dateStop.setText(targetDateFormat2.format(date2));

                            } else {
                                Logger.d(TAG, modths[0]);
//                                holder.dateStart.setText(modths[0]);
                                String dateFormat = "dd-MM";
                                SimpleDateFormat sourceDateFormat = new SimpleDateFormat("dd-MM");
                                Date date1 = sourceDateFormat.parse(modths[0].toString());
                                SimpleDateFormat targetDateFormat = new SimpleDateFormat(dateFormat);
                                holder.dateStart.setText(targetDateFormat.format(date1));
                            }
                        } else {
                        }
                        setViewDefine(holder, seasons, viewType);
                        break;
                    case TYPE_OVERLOOK:
                        OverLook overlook = (OverLook) entities;
                        if (!TextUtils.isEmpty(overlook.title)) {
                            holder.title.setText(overlook.title);
                        } else {

                        }
                        if (!TextUtils.isEmpty(overlook.overlook)) {
                            String[] strOverlook = overlook.overlook.trim().split(",");
                            if (strOverlook.length >= 2) {
                                Logger.d(TAG, strOverlook[0] + " -> " + strOverlook[1]);
                                holder.dateStart.setText(strOverlook[0]);
                                holder.dateStop.setText(strOverlook[1]);
                            } else {
                                Logger.d(TAG, strOverlook[0]);
                                holder.dateStart.setText(strOverlook[0]);
                            }
                        } else {

                        }
                        setViewDefine(holder, overlook, viewType);
                        holder.btnRemove.setOnClickListener(new Remove(TYPE_OVERLOOK, overlook.overlookDayID));
                        break;
                    case TYPE_ADD:
                        final Alarm alarm = (Alarm) entities;
                        if (!TextUtils.isEmpty(alarm.title)) {
                            holder.title.setText(alarm.title);
                        } else {

                        }
                        setViewAlaramDefaul(holder);
                        if (alarm.objs != null) {
                            if (alarm.objs.size() > 0) {
                                RepeatWeekdays weekdays = alarm.objs.get(0);
                                if (!TextUtils.isEmpty(weekdays.repeatWeekday)) {
                                    String[] repeat = weekdays.repeatWeekday.trim().split(",");
                                    for (int i = 0; i < repeat.length; i++) {
                                        setViewRepeat(holder, repeat[i].toString());
                                    }
                                } else {
                                    Logger.d(TAG, "isEmpty nullllllllllllllllllllllll");
                                }

                            } else {

                            }

                        } else {
                            Logger.d(TAG, "objs nullllllllllllllllllllllll");
                        }

                        setViewSeason(holder, alarm.seasonsID);
                        if (!TextUtils.isEmpty(alarm.music)) {
                            holder.music.setText(alarm.music);
                        } else {

                        }

                        if (!TextUtils.isEmpty(alarm.alarmTimer)) {
                            holder.dateStart.setText(alarm.alarmTimer);
                        } else {

                        }

                        if (!TextUtils.isEmpty(alarm.alarmStop)) {
                            holder.dateStop.setText(alarm.alarmStop);
                        } else {

                        }
                        boolean check = alarm.isRun == 1 ? true : false;
                        holder.cbxRun.setChecked(check);

                        setViewDefine(holder, alarm, viewType);
                        holder.btnRemove.setOnClickListener(new Remove(TYPE_ADD, alarm.alarmID));

                        holder.cbxRun.setOnClickListener(new TimeRepeat(alarm, holder));

                        holder.t2.setOnClickListener(new TimeRepeat(alarm, holder));
                        holder.t3.setOnClickListener(new TimeRepeat(alarm, holder));
                        holder.t4.setOnClickListener(new TimeRepeat(alarm, holder));
                        holder.t5.setOnClickListener(new TimeRepeat(alarm, holder));
                        holder.t6.setOnClickListener(new TimeRepeat(alarm, holder));
                        holder.t7.setOnClickListener(new TimeRepeat(alarm, holder));
                        holder.cn.setOnClickListener(new TimeRepeat(alarm, holder));

                        holder.muaXuan.setOnClickListener(new TimeRepeat(alarm, holder));
                        holder.muaHa.setOnClickListener(new TimeRepeat(alarm, holder));
                        holder.muaThu.setOnClickListener(new TimeRepeat(alarm, holder));
                        holder.muadong.setOnClickListener(new TimeRepeat(alarm, holder));

                        holder.music.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    iDialogSetting.onPickMusic(alarm, DialogSettingAdapter.this);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        break;
                }

                holder.title.setOnEditorActionListener(new TextView.OnEditorActionListener() {

                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                        if (actionId == R.id.cantidad || actionId == EditorInfo.IME_NULL) {
                            switch (viewType) {
                                case TYPE_OVERLOOK:
                                    OverLookModel overLookModel = new OverLookModel(context);
                                    OverLook overlook = (OverLook) entities;
                                    overlook.title = holder.title.getText().toString().trim();
                                    if (overLookModel.update(overlook)) {
                                        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                                        imm.hideSoftInputFromWindow(holder.title.getRootView().getWindowToken(), 0);
                                        Snackbar.make(holder.title.getRootView(), "update...", Snackbar.LENGTH_SHORT).show();
                                    }
                                    break;
                                case TYPE_ADD:
                                    AlarmModel alarmModel = new AlarmModel(context);
                                    Alarm alarm = (Alarm) entities;
                                    alarm.title = holder.title.getText().toString().trim();
                                    if (alarmModel.update(alarm)) {
                                        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                                        imm.hideSoftInputFromWindow(holder.title.getRootView().getWindowToken(), 0);
                                        Snackbar.make(holder.title.getRootView(), "update...", Snackbar.LENGTH_SHORT).show();
                                    }
                                    break;
                            }
                            return true;
                        }
                        return false;
                    }
                });
            }
        } catch (
                Exception e
                )

        {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemViewType(int position) {
        try {
            if (data != null) {
                return data.get(position).viewType;
            } else {
                return TYPE_NULL;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return TYPE_NULL;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public List<BaseAlarmEntities> getData() {
        return data;
    }

    public void setData(List<BaseAlarmEntities> data) {
        this.data = data;
    }

    private void setViewRepeat(ViewHolder holder, String repeat) {
        Logger.d(TAG, repeat);
        switch (repeat.toLowerCase().trim()) {
            case "t2":
                holder.t2.setChecked(true);
                break;
            case "t3":
                holder.t3.setChecked(true);
                break;
            case "t4":
                holder.t4.setChecked(true);
                break;
            case "t5":
                holder.t5.setChecked(true);
                break;
            case "t6":
                holder.t6.setChecked(true);
                break;
            case "t7":
                holder.t7.setChecked(true);
                break;
            case "cn":
                holder.cn.setChecked(true);
                break;
            default:
                break;
        }
    }

    public void setViewRepeatDefaul(ViewHolder holder) {
        holder.t2.setChecked(false);
        holder.t3.setChecked(false);
        holder.t4.setChecked(false);
        holder.t5.setChecked(false);
        holder.t6.setChecked(false);
        holder.t7.setChecked(false);
        holder.cn.setChecked(false);
    }

    public void setViewAlaramDefaul(ViewHolder holder) {
        setViewRepeatDefaul(holder);
        holder.music.setText("Chưa có");
    }

    public void setViewDefaul(ViewHolder holder) {
        holder.title.setText(null);
        holder.dateStart.setText(null);
        holder.dateStop.setText(null);
    }

    private void setViewSeason(ViewHolder holder, int id) {
        switch (id) {
            case 1:
                holder.muaXuan.setChecked(true);
                holder.muaHa.setChecked(false);
                holder.muaThu.setChecked(false);
                holder.muadong.setChecked(false);
                break;
            case 2:
                holder.muaXuan.setChecked(false);
                holder.muaHa.setChecked(true);
                holder.muaThu.setChecked(false);
                holder.muadong.setChecked(false);
                break;
            case 3:
                holder.muaXuan.setChecked(false);
                holder.muaHa.setChecked(false);
                holder.muaThu.setChecked(true);
                holder.muadong.setChecked(false);
                break;
            case 4:
                holder.muaXuan.setChecked(false);
                holder.muaHa.setChecked(false);
                holder.muaThu.setChecked(false);
                holder.muadong.setChecked(true);
                break;
            default:
                break;
        }
    }

    private void setViewDefine(final ViewHolder holder, final BaseAlarmEntities obj, final int type) {
        if (type != TYPE_ADD) {
            holder.dateStart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogDatePicker datePicker = new DialogDatePicker(context, DialogSettingAdapter.this, holder, holder.dateStart, obj, type);
                    datePicker.show();
                }
            });

            holder.dateStop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogDatePicker datePicker = new DialogDatePicker(context, DialogSettingAdapter.this, holder, holder.dateStop, obj, type);
                    datePicker.show();
                }
            });
        } else {
            holder.dateStart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogTimePiker datePicker = new DialogTimePiker(context, holder.dateStart.getText().toString(), DialogSettingAdapter.this, holder, holder.dateStart, obj, type);
                    datePicker.show();
                }
            });

            holder.dateStop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogTimePiker datePicker = new DialogTimePiker(context, holder.dateStop.getText().toString(), DialogSettingAdapter.this, holder, holder.dateStop, obj, type) {
                    };
                    datePicker.show();
                }
            });
        }
    }

    @Override
    public void onDatePicker(Calendar date, DialogSettingAdapter.ViewHolder holder, View view, BaseAlarmEntities entities, int type) throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM");
        String d = format.format(date.getTime());
        ((CheckedTextView) view).setText(d);

        switch (type) {
            case TYPE_SETTING_SEASON:
                Seasons seasons = (Seasons) entities;
                if (!TextUtils.isEmpty(seasons.months)) {
                    String[] strOverLook = seasons.months.split(",");

                    if (strOverLook.length >= 2) {
                        if (view.getId() == R.id.dialog_setting_define_date_start) {
                            seasons.months = ((CheckedTextView) view).getText().toString() + "," + strOverLook[1];
                        } else {
                            seasons.months = strOverLook[0] + "," + ((CheckedTextView) view).getText().toString();
                        }
                    } else {
                        if (view.getId() == R.id.dialog_setting_define_date_start) {
                            seasons.months = ((CheckedTextView) view).getText().toString() + ",";
                        } else {
                            seasons.months = strOverLook[0] + "," + ((CheckedTextView) view).getText().toString();
                        }
                    }

                } else {
                    seasons.months = ((CheckedTextView) view).getText().toString();
                }

                SeasonsModel seasonsModel = new SeasonsModel(context);
                if (seasonsModel.update(seasons)) {
                    Snackbar.make(view, "Bạn đã thay đổi thời gian " + ((CheckedTextView) view).getText().toString(), Snackbar.LENGTH_LONG).show();
                } else {
                    Snackbar.make(view, "Có lỗi không thể cập nhật thời gian", Snackbar.LENGTH_LONG).show();
                }
                break;
            case TYPE_OVERLOOK:
                OverLook overlook = (OverLook) entities;
                if (!TextUtils.isEmpty(overlook.overlook)) {
                    String[] strOverLook = overlook.overlook.split(",");

                    if (strOverLook.length >= 2) {
                        if (view.getId() == R.id.dialog_setting_define_date_start) {
                            overlook.overlook = ((CheckedTextView) view).getText().toString() + "," + strOverLook[1];
                        } else {
                            overlook.overlook = strOverLook[0] + "," + ((CheckedTextView) view).getText().toString();
                        }
                    } else {
                        if (view.getId() == R.id.dialog_setting_define_date_start) {
                            overlook.overlook = ((CheckedTextView) view).getText().toString() + ",";
                        } else {
                            overlook.overlook = strOverLook[0] + "," + ((CheckedTextView) view).getText().toString();
                        }
                    }

                } else {
                    overlook.overlook = ((CheckedTextView) view).getText().toString();
                }

                OverLookModel overLookModel = new OverLookModel(context);
                if (overLookModel.update(overlook)) {
                    Snackbar.make(view, "Bạn đã thay đổi thời gian " + ((CheckedTextView) view).getText().toString(), Snackbar.LENGTH_LONG).show();
                } else {
                    Snackbar.make(view, "Có lỗi không thể cập nhật thời gian", Snackbar.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onTimePicker(String time, ViewHolder holder, View view, BaseAlarmEntities entities, int type) throws Exception {
        Alarm alarm = (Alarm) entities;
        ((CheckedTextView) view).setText(time);

        if (view.getId() == R.id.dialog_setting_define_date_start) {
            alarm.alarmTimer = ((CheckedTextView) view).getText().toString();
        } else {
            alarm.alarmStop = ((CheckedTextView) view).getText().toString();
        }

        AlarmModel model = new AlarmModel(context);
        if (model.update(alarm)) {
            Snackbar.make(view, "Bạn đã thay đổi thời gian " + ((CheckedTextView) view).getText().toString(), Snackbar.LENGTH_LONG).show();
        } else {
            Snackbar.make(view, "Có lỗi không thể cập nhật thời gian", Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View v) {

    }

    public class TimeRepeat implements View.OnClickListener {
        private Alarm alarm;
        private ViewHolder holder;

        public TimeRepeat(Alarm alarm, ViewHolder holder) {
            this.alarm = alarm;
            this.holder = holder;
        }

        @Override
        public void onClick(View v) {
            try {
                AlarmModel model = new AlarmModel(context);
                switch (v.getId()) {
                    case R.id.isRun:
                        alarm.isRun = holder.cbxRun.isChecked() ? 1 : 0;
                        if (model.update(alarm)) {
                            Snackbar.make(v, "update...", Snackbar.LENGTH_LONG).show();
                        } else {
                            Snackbar.make(v, "Không xác định.!", Snackbar.LENGTH_LONG).show();
                        }
                        break;
                    case R.id.t2:
                        Logger.d(TAG, getRepeat(holder));
                        alarm.objs.get(0).repeatWeekday = getRepeat(holder);
                        if (model.updateRepeat(alarm.objs.get(0))) {
                            Snackbar.make(v, "update...", Snackbar.LENGTH_LONG).show();
                        } else {
                            Snackbar.make(v, "Không xác định.!", Snackbar.LENGTH_LONG).show();
                        }
                        break;
                    case R.id.t3:
                        Logger.d(TAG, getRepeat(holder));
                        alarm.objs.get(0).repeatWeekday = getRepeat(holder);
                        if (model.updateRepeat(alarm.objs.get(0))) {
                            Snackbar.make(v, "update...", Snackbar.LENGTH_LONG).show();
                        } else {
                            Snackbar.make(v, "Không xác định.!", Snackbar.LENGTH_LONG).show();
                        }
                        break;
                    case R.id.t4:
                        Logger.d(TAG, getRepeat(holder));
                        alarm.objs.get(0).repeatWeekday = getRepeat(holder);
                        if (model.updateRepeat(alarm.objs.get(0))) {
                            Snackbar.make(v, "update...", Snackbar.LENGTH_LONG).show();
                        } else {
                            Snackbar.make(v, "Không xác định.!", Snackbar.LENGTH_LONG).show();
                        }
                        break;
                    case R.id.t5:
                        Logger.d(TAG, getRepeat(holder));
                        alarm.objs.get(0).repeatWeekday = getRepeat(holder);
                        if (model.updateRepeat(alarm.objs.get(0))) {
                            Snackbar.make(v, "update...", Snackbar.LENGTH_LONG).show();
                        } else {
                            Snackbar.make(v, "Không xác định.!", Snackbar.LENGTH_LONG).show();
                        }
                        break;
                    case R.id.t6:
                        Logger.d(TAG, getRepeat(holder));
                        alarm.objs.get(0).repeatWeekday = getRepeat(holder);
                        if (model.updateRepeat(alarm.objs.get(0))) {
                            Snackbar.make(v, "update...", Snackbar.LENGTH_LONG).show();
                        } else {
                            Snackbar.make(v, "Không xác định.!", Snackbar.LENGTH_LONG).show();
                        }
                        break;
                    case R.id.t7:
                        Logger.d(TAG, getRepeat(holder));
                        alarm.objs.get(0).repeatWeekday = getRepeat(holder);
                        if (model.updateRepeat(alarm.objs.get(0))) {
                            Snackbar.make(v, "update...", Snackbar.LENGTH_LONG).show();
                        } else {
                            Snackbar.make(v, "Không xác định.!", Snackbar.LENGTH_LONG).show();
                        }
                        break;
                    case R.id.cn:
                        Logger.d(TAG, getRepeat(holder));
                        alarm.objs.get(0).repeatWeekday = getRepeat(holder);
                        if (model.updateRepeat(alarm.objs.get(0))) {
                            Snackbar.make(v, "update...", Snackbar.LENGTH_LONG).show();
                        } else {
                            Snackbar.make(v, "Không xác định.!", Snackbar.LENGTH_LONG).show();
                        }
                        break;
                    case R.id.muaXuan:
                        alarm.seasonsID = 1;
                        if (holder.muaXuan.isChecked()) {
                            holder.muaHa.setChecked(false);
                            holder.muaThu.setChecked(false);
                            holder.muadong.setChecked(false);
                        }
                        if (model.update(alarm)) {
                            Snackbar.make(v, "update...", Snackbar.LENGTH_LONG).show();
                        } else {
                            Snackbar.make(v, "Không xác định.!", Snackbar.LENGTH_LONG).show();
                        }
                        break;
                    case R.id.muaHa:
                        alarm.seasonsID = 2;
                        if (holder.muaHa.isChecked()) {
                            holder.muaXuan.setChecked(false);
                            holder.muaThu.setChecked(false);
                            holder.muadong.setChecked(false);
                        }
                        if (model.update(alarm)) {
                            Snackbar.make(v, "update...", Snackbar.LENGTH_LONG).show();
                        } else {
                            Snackbar.make(v, "Không xác định.!", Snackbar.LENGTH_LONG).show();
                        }
                        break;
                    case R.id.muaThu:
                        alarm.seasonsID = 3;
                        if (holder.muaThu.isChecked()) {
                            holder.muaHa.setChecked(false);
                            holder.muaXuan.setChecked(false);
                            holder.muadong.setChecked(false);
                        }
                        if (model.update(alarm)) {
                            Snackbar.make(v, "update...", Snackbar.LENGTH_LONG).show();
                        } else {
                            Snackbar.make(v, "Không xác định.!", Snackbar.LENGTH_LONG).show();
                        }
                        break;
                    case R.id.muadong:
                        alarm.seasonsID = 4;
                        if (holder.muadong.isChecked()) {
                            holder.muaHa.setChecked(false);
                            holder.muaThu.setChecked(false);
                            holder.muaXuan.setChecked(false);
                        }
                        if (model.update(alarm)) {
                            Snackbar.make(v, "update...", Snackbar.LENGTH_LONG).show();
                        } else {
                            Snackbar.make(v, "Không xác định.!", Snackbar.LENGTH_LONG).show();
                        }
                        break;
                    default:
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
                List<BaseAlarmEntities> oldData = DialogSettingAdapter.this.getData();
                if (oldData == null) {
                    oldData = new ArrayList<>();
                } else {
                    oldData.clear();
                }
                AlarmModel alarmModel = new AlarmModel(context);
                List<Alarm> newDataAlarmModel = alarmModel.getAll();
                for (int i = 0; i < newDataAlarmModel.size(); i++) {
                    newDataAlarmModel.get(i).viewType = TYPE_ADD;
                }
                oldData.addAll(newDataAlarmModel);
                DialogSettingAdapter.this.notifyDataSetChanged();
            }
        }
    }

    public String getRepeat(ViewHolder holder) {
        String repeat = "";
        for (int i = 0; i < 7; i++) {
            switch (i) {
                case 0:
                    repeat += holder.t2.isChecked() ? "t2," : "";
                    break;
                case 1:
                    repeat += holder.t3.isChecked() ? "t3," : "";
                    break;
                case 2:
                    repeat += holder.t4.isChecked() ? "t4," : "";
                    break;
                case 3:
                    repeat += holder.t5.isChecked() ? "t5," : "";
                    break;
                case 4:
                    repeat += holder.t6.isChecked() ? "t6," : "";
                    break;
                case 5:
                    repeat += holder.t7.isChecked() ? "t7," : "";
                    break;
                case 6:
                    repeat += holder.cn.isChecked() ? "cn" : "";
                    break;
            }
        }
//        Logger.d(TAG, repeat.charAt(repeat.length() - 1) + "");
        if (",".equals(repeat.charAt(repeat.length() - 1) + "")) {
            repeat = repeat.substring(0, repeat.lastIndexOf(","));
        }
        return repeat;
    }

    public class Remove implements View.OnClickListener {
        private int type;
        private int id;

        public Remove(int type, int id) {
            this.type = type;
            this.id = id;
        }

        @Override
        public void onClick(View v) {
            try {
                iDialogSetting.onRemove(id);
            } catch (Exception e) {
                e.printStackTrace();
            }

            DialogSettingAdapter.this.notifyDataSetChanged();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private EditText title;
        private CheckedTextView dateStart;
        private CheckedTextView dateStop;
        private Button btnRemove;

        private CheckBox cbxRun;
        private ToggleButton t2, t3, t4, t5, t6, t7, cn;
        private ToggleButton muaXuan, muaHa, muaThu, muadong;
        private CheckedTextView music;

        public ViewHolder(View v) {
            super(v);
            title = (EditText) v.findViewById(R.id.dialog_setting_title);
            dateStart = (CheckedTextView) v.findViewById(R.id.dialog_setting_define_date_start);
            dateStop = (CheckedTextView) v.findViewById(R.id.dialog_setting_define_date_stop);
            btnRemove = (Button) v.findViewById(R.id.dialog_setting_remove);

            cbxRun = (CheckBox) v.findViewById(R.id.isRun);
            t2 = (ToggleButton) v.findViewById(R.id.t2);
            t3 = (ToggleButton) v.findViewById(R.id.t3);
            t4 = (ToggleButton) v.findViewById(R.id.t4);
            t5 = (ToggleButton) v.findViewById(R.id.t5);
            t6 = (ToggleButton) v.findViewById(R.id.t6);
            t7 = (ToggleButton) v.findViewById(R.id.t7);
            cn = (ToggleButton) v.findViewById(R.id.cn);

            muaXuan = (ToggleButton) v.findViewById(R.id.muaXuan);
            muaHa = (ToggleButton) v.findViewById(R.id.muaHa);
            muaThu = (ToggleButton) v.findViewById(R.id.muaThu);
            muadong = (ToggleButton) v.findViewById(R.id.muadong);

            music = (CheckedTextView) v.findViewById(R.id.dialog_setting_music);
        }
    }

}