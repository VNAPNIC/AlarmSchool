package vnapnic.project.alarmschool.views.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import vnapnic.project.alarmschool.R;
import vnapnic.project.alarmschool.activities.MainActivity;
import vnapnic.project.alarmschool.common.Instance;
import vnapnic.project.alarmschool.common.Logger;
import vnapnic.project.alarmschool.controller.activities.IMainActivities;
import vnapnic.project.alarmschool.controller.fragments.IDialogSetting;
import vnapnic.project.alarmschool.model.AlarmModel;
import vnapnic.project.alarmschool.model.OverLookModel;
import vnapnic.project.alarmschool.model.SeasonsModel;
import vnapnic.project.alarmschool.model.entities.Alarm;
import vnapnic.project.alarmschool.model.entities.BaseAlarmEntities;
import vnapnic.project.alarmschool.model.entities.OverLook;
import vnapnic.project.alarmschool.model.entities.RepeatWeekdays;
import vnapnic.project.alarmschool.model.entities.Seasons;
import vnapnic.project.alarmschool.views.adapter.DialogSettingAdapter;

/**
 * Created by hainam1421 on 1/28/2016.
 */
public class DialogSetting extends Dialog implements Instance, IDialogSetting {

    private final String TAG = DialogSetting.class.getName();
    private DialogSettingAdapter adapter;
    private IMainActivities iMainActivities;
    private RecyclerView listView;
    private Button addNew, cancel;
    private View root;
    private int type;

    public DialogSetting(Context context, int type, IMainActivities iMainActivities) {
        super(context);
        try {
            root = getLayoutInflater().inflate(R.layout.dialog_setting, null, false);
            this.setContentView(root);
            this.setTitle("Cài đặt");
            this.type = type;
            this.iMainActivities = iMainActivities;
            initialize(context, type);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initialize(final Context context, final int type) throws Exception {
        listView = (RecyclerView) root.findViewById(R.id.DialogSetting);
        addNew = (Button) root.findViewById(R.id.dialogAddnew);
        cancel = (Button) root.findViewById(R.id.dialogCancel);
        listView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new DialogSettingAdapter(context, this);
        listView.setAdapter(adapter);
        addNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
//                    ((MainActivity) context).update();
                    switch (type) {
                        case TYPE_OVERLOOK:
                            OverLookModel model = new OverLookModel(getContext());
                            if (model.add(new OverLook())) {
                                setDataSeason(TYPE_OVERLOOK);
                                Snackbar.make(getCurrentFocus(), "Thêm mới thành công.!", Snackbar.LENGTH_SHORT).show();
                                listView.scrollToPosition(adapter.getItemCount() - 1);
                            } else {
                                Snackbar.make(getCurrentFocus(), "Lỗi không xác định.!", Snackbar.LENGTH_SHORT).show();
                            }
                            break;
                        case TYPE_ADD:
                            AlarmModel alarmModel = new AlarmModel(getContext());
                            if (alarmModel.add(new Alarm())) {
                                if (alarmModel.addRepeat(new RepeatWeekdays())) {
                                    setDataSeason(TYPE_ADD);
                                    Snackbar.make(getCurrentFocus(), "Thêm mới thành công.!", Snackbar.LENGTH_SHORT).show();
                                    listView.scrollToPosition(adapter.getItemCount() - 1);
                                } else {
                                    Snackbar.make(getCurrentFocus(), "Lỗi không xác định.!", Snackbar.LENGTH_SHORT).show();
                                }
                            } else {
                                Snackbar.make(getCurrentFocus(), "Lỗi không xác định.!", Snackbar.LENGTH_SHORT).show();
                            }
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (type == TYPE_SETTING_SEASON) {
            addNew.setVisibility(View.GONE);

        }
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogSetting.this.dismiss();
            }
        });

        switch (type) {
            case TYPE_SETTING_SEASON:
                Logger.d(TAG, "TYPE_SETTING_SEASON");
                setDataSeason(type);
                break;
            case TYPE_OVERLOOK:
                Logger.d(TAG, "TYPE_OVERLOOK");
                setDataSeason(type);
                break;
            case TYPE_ADD:
                Logger.d(TAG, "TYPE_ADD");
                setDataSeason(type);
                break;
        }
    }

    private void setDataSeason(int type) {
        List<BaseAlarmEntities> oldData = adapter.getData();
        if (oldData == null) {
            oldData = new ArrayList<>();
        } else {
            oldData.clear();
        }
        switch (type) {
            case TYPE_SETTING_SEASON:
                SeasonsModel seasonsModel = new SeasonsModel(getContext());
                List<Seasons> newDataSeasonses = seasonsModel.getAll();
                for (int i = 0; i < newDataSeasonses.size(); i++) {
                    newDataSeasonses.get(i).viewType = type;
                }
                oldData.addAll(newDataSeasonses);
                adapter.notifyDataSetChanged();
                break;
            case TYPE_OVERLOOK:
                OverLookModel overLookModel = new OverLookModel(getContext());
                List<OverLook> newDataOverLooks = overLookModel.getAll();
                for (int i = 0; i < newDataOverLooks.size(); i++) {
                    newDataOverLooks.get(i).viewType = type;
                }
                oldData.addAll(newDataOverLooks);
                adapter.notifyDataSetChanged();
                break;
            case TYPE_ADD:
                AlarmModel alarmModel = new AlarmModel(getContext());
                List<Alarm> newDataAlarmModel = alarmModel.getAll();
                for (int i = 0; i < newDataAlarmModel.size(); i++) {
                    newDataAlarmModel.get(i).viewType = type;
                }
                oldData.addAll(newDataAlarmModel);
                adapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public void onRemove(int id) throws Exception {
        switch (type) {
            case TYPE_OVERLOOK:
                OverLookModel model = new OverLookModel(getContext());
                if (model.remove(id)) {
                    Snackbar.make(getCurrentFocus(), "Bạn đã xóa 1 Alarm", Snackbar.LENGTH_LONG).show();
                    setDataSeason(type);
                }
                break;
            case TYPE_ADD:
                AlarmModel alarmModel = new AlarmModel(getContext());
                if (alarmModel.remove(id)) {
                    Snackbar.make(getCurrentFocus(), "Bạn đã xóa 1 Alarm", Snackbar.LENGTH_LONG).show();
                    setDataSeason(type);
                }
                break;
        }
    }

    @Override
    public void onPickMusic(Alarm alarm, DialogSettingAdapter adapter) throws Exception {
        iMainActivities.onPickMusic(alarm, adapter);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        try {
            ((MainActivity) scanForActivity(getContext())).onUpdateUI();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static AppCompatActivity scanForActivity(Context cont) {
        if (cont == null)
            return null;
        else if (cont instanceof AppCompatActivity)
            return (AppCompatActivity) cont;
        else if (cont instanceof ContextWrapper)
            return scanForActivity(((ContextWrapper) cont).getBaseContext());

        return null;
    }
}
