package vnapnic.project.alarmschool.views.adapter;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import vnapnic.project.alarmschool.R;
import vnapnic.project.alarmschool.base.adapter.BaseAdapter;
import vnapnic.project.alarmschool.common.Logger;
import vnapnic.project.alarmschool.model.AlarmModel;
import vnapnic.project.alarmschool.model.entities.Alarm;
import vnapnic.project.alarmschool.model.entities.RepeatWeekdays;
import vnapnic.project.alarmschool.model.entities.Seasons;
import vnapnic.project.alarmschool.views.viewholder.AlarmViewHolder;
import vnapnic.project.alarmschool.views.viewholder.SeasonViewHolder;

/**
 * Created by hainam1421 on 1/20/2016.
 */
public class AlarmListAdapter extends BaseAdapter {

    private final String TAG = ClockListAdapter.class.getName();

    public AlarmListAdapter(Context context) {
        super(context);
    }

    @Override
    public View getGroupView(final int position, boolean isExpanded, View v, ViewGroup parent) {
        try {

            SeasonViewHolder seasonViewHolder;
            if (v == null) {
                seasonViewHolder = new SeasonViewHolder();
                v = inflater.inflate(R.layout.seasons_item, parent, false);
                seasonViewHolder.title = (CheckedTextView) v.findViewById(R.id.season_title);
//                seasonViewHolder.isRun = (SwitchCompat) v.findViewById(R.id.season_run);
                v.setTag(seasonViewHolder);
            } else {
                seasonViewHolder = (SeasonViewHolder) v.getTag();
            }

            Seasons obj = objs.get(position);
            boolean check = obj.isRun == 1 ? true : false;

            String months = TextUtils.isEmpty(obj.months) ? "NULL" : obj.months;
            if (!"NULL".equals(months)) {
                String[] getData = obj.months.split(",");
                if(getData.length >1) {
                    String dateFormat = "dd-MM";
                    SimpleDateFormat sourceDateFormat = new SimpleDateFormat("dd-MM");

                    Date date1 = sourceDateFormat.parse(getData[0].toString());
                    SimpleDateFormat targetDateFormat = new SimpleDateFormat(dateFormat);
                    months = targetDateFormat.format(date1);

                    Date date2 = sourceDateFormat.parse(getData[1].toString());
                    SimpleDateFormat targetDateFormat2 = new SimpleDateFormat(dateFormat);

                    months += "," + targetDateFormat2.format(date2);
                }

            }
            if (TextUtils.isEmpty(obj.months) || !check) {

                String valueHTMl = obj.title + " (Stop)\n" + months;
                Logger.d(TAG, valueHTMl);
                seasonViewHolder.title.setText(valueHTMl, TextView.BufferType.SPANNABLE);
            } else {
                String valueHTMl = obj.title + " (Start)\n" + months;
                Logger.d(TAG, valueHTMl);
                seasonViewHolder.title.setText(valueHTMl, TextView.BufferType.SPANNABLE);
            }

            return v;
        } catch (Exception e) {
            e.printStackTrace();
            v = new View(context);
            return v;
        }
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View v, ViewGroup parent) {
        try {
            final AlarmViewHolder alarmViewHolder;
            if (v == null) {
                alarmViewHolder = new AlarmViewHolder();
                v = inflater.inflate(R.layout.alarm_item, null);
                alarmViewHolder.timer = (TextView) v.findViewById(R.id.alarm_timer);
                alarmViewHolder.title = (TextView) v.findViewById(R.id.alarm_title);
                alarmViewHolder.weekday = (TextView) v.findViewById(R.id.alarm_weekday);
                alarmViewHolder.music = (TextView) v.findViewById(R.id.alarm_music);
                alarmViewHolder.isRun = (SwitchCompat) v.findViewById(R.id.alarm_run);
                v.setTag(alarmViewHolder);
            } else {
                alarmViewHolder = (AlarmViewHolder) v.getTag();
            }

            final Alarm obj = objs.get(groupPosition).objs.get(childPosition);

            if (TextUtils.isEmpty(obj.alarmTimer)) {
                alarmViewHolder.timer.setText("00:00");
            } else {
                alarmViewHolder.timer.setText(obj.alarmTimer);
            }
            if (TextUtils.isEmpty(obj.title)) {
                alarmViewHolder.title.setText("error");
            } else {
                alarmViewHolder.title.setText(obj.title);
            }
            if (obj.objs != null) {
                RepeatWeekdays weekdays = obj.objs.get(0);
                if (obj.objs.size() > 0) {
                    if (TextUtils.isEmpty(weekdays.repeatWeekday)) {
                        alarmViewHolder.weekday.setText(weekdays.repeatWeekday);
                    }
                }
            }

            if (TextUtils.isEmpty(obj.music)) {
                alarmViewHolder.music.setText("NULL");
            } else {
                alarmViewHolder.music.setText(obj.music);
            }
            boolean check = obj.isRun == 1 ? true : false;
            alarmViewHolder.isRun.setChecked(check);
            alarmViewHolder.isRun.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlarmModel model = new AlarmModel(context);
                    obj.isRun = alarmViewHolder.isRun.isChecked() ? 1 : 0;
                    if (model.update(obj)) {
                        Snackbar.make(v, "update...", Snackbar.LENGTH_LONG).show();
                    } else {
                        Snackbar.make(v, "Không xác định.!", Snackbar.LENGTH_LONG).show();
                    }
                }
            });
            return v;
        } catch (Exception e) {
            e.printStackTrace();
            v = new View(context);
            return v;
        }
    }
}