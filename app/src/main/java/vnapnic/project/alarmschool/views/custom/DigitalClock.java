package vnapnic.project.alarmschool.views.custom;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import vnapnic.project.alarmschool.common.Instance;

/**
 * Created by hainam1421 on 3/4/2016.
 */
public class DigitalClock extends TextView implements Instance {
    private Timer clockTimer;
    private final TimerTask clockTask = new TimerTask() {
        @Override
        public void run() {
            mHandler.post(mUpdateResults);
        }
    };

    final Handler mHandler = new Handler();
    final Runnable mUpdateResults = new Runnable() {
        public void run() {
            update();
        }
    };

    public DigitalClock(Context context) {
        super(context);
        init();
    }

    private void update() {
        setText(Datetime());
    }

    public String Datetime() {
        Calendar c = Calendar.getInstance();
//        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss\n" +
                "dd-MM-yyyy");
        return df.format(c.getTime());
    }

    private void init() {
        clockTimer = new Timer();
        clockTimer.scheduleAtFixedRate(clockTask, 0, 1000);
    }

    public DigitalClock(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DigitalClock(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

}