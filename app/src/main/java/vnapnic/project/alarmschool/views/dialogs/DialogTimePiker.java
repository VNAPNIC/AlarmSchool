package vnapnic.project.alarmschool.views.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;

import vnapnic.project.alarmschool.R;
import vnapnic.project.alarmschool.controller.IDialogDatePiker;
import vnapnic.project.alarmschool.controller.IDialogTimePiker;
import vnapnic.project.alarmschool.model.entities.BaseAlarmEntities;
import vnapnic.project.alarmschool.views.adapter.DialogSettingAdapter;
import vnapnic.project.alarmschool.widget.MonthView;

/**
 * Created by hainam1421 on 1/31/2016.
 */
public class DialogTimePiker extends Dialog {

    private final String TAG = DialogTimePiker.class.getName();
    private View root;
    private TimePicker mTimePicker;
    private int type;
    private DialogSettingAdapter.ViewHolder holder;
    private View view;
    private BaseAlarmEntities obj;
    private IDialogTimePiker timePiker;
    private Button done;

    public DialogTimePiker(Context context, String time, IDialogTimePiker timePiker, DialogSettingAdapter.ViewHolder holder, View view, BaseAlarmEntities obj, int type) {
        super(context);
        try {
            this.timePiker = timePiker;
            this.holder = holder;
            this.view = view;
            this.type = type;
            this.obj = obj;
            root = getLayoutInflater().inflate(R.layout.dialog_time_piker, null, false);
            this.setContentView(root);
            mTimePicker = (TimePicker) root.findViewById(R.id.timePicker);
            mTimePicker.setIs24HourView(true);

            if (!TextUtils.isEmpty(time)) {
                String[] strTime = time.split(":");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    mTimePicker.setHour(Integer.parseInt(strTime[0]));
                    mTimePicker.setMinute(Integer.parseInt(strTime[1]));
                } else {
                    mTimePicker.setCurrentHour(Integer.parseInt(strTime[0]));
                    mTimePicker.setCurrentMinute(Integer.parseInt(strTime[1]));
                }
            } else {

            }

            done = (Button) root.findViewById(R.id.dialog_time_picker_done);
            initialize(context);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initialize(Context context) throws Exception {
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    int hour;
                    int minute;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                        hour = mTimePicker.getHour();
                        minute = mTimePicker.getMinute();
                    } else {
                        hour = mTimePicker.getCurrentHour();
                        minute = mTimePicker.getCurrentMinute();
                    }
                    String h = hour + "";
                    String m = minute + "";
                    if (hour <= 9) {
                        h = "0" + hour;
                    }
                    if (minute <= 9) {
                        m = "0" + minute;
                    }
                    String time = h + ":" + m + ":00";
                    timePiker.onTimePicker(time, holder, view, obj, type);
                    DialogTimePiker.this.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
