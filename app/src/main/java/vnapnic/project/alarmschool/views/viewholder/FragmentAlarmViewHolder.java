package vnapnic.project.alarmschool.views.viewholder;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import vnapnic.project.alarmschool.R;
import vnapnic.project.alarmschool.base.viewholder.BaseFragmentViewHolder;
import vnapnic.project.alarmschool.common.Logger;
import vnapnic.project.alarmschool.controller.fragments.IFragmentAlarm;
import vnapnic.project.alarmschool.model.SeasonsModel;
import vnapnic.project.alarmschool.model.entities.Seasons;
import vnapnic.project.alarmschool.views.adapter.AlarmListAdapter;
import vnapnic.project.alarmschool.widget.MonthView;

/**
 * Created by hainam1421 on 1/21/2016.
 */
public class FragmentAlarmViewHolder extends BaseFragmentViewHolder{

    private final String TAG = FragmentAlarmViewHolder.class.getName();
    private ExpandableListView alarmList;
    private SeasonsModel model;
    private IFragmentAlarm iFragmentAlarm;
    private AlarmListAdapter adapter;

    public FragmentAlarmViewHolder(Activity activity, View root, IFragmentAlarm iFragmentAlarm) throws Exception {
        super(activity, root, iFragmentAlarm);

        this.iFragmentAlarm = iFragmentAlarm;
        alarmList = (ExpandableListView) root.findViewById(R.id.alarmList);
        model = new SeasonsModel(activity);

        initialize(activity);
    }

    @Override
    protected void initialize(Context context) throws Exception {
        alarmList.setDividerHeight(2);
        alarmList.setGroupIndicator(null);
        alarmList.setClickable(true);
        adapter = new AlarmListAdapter(context);
        adapter.setInflater(
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE),
                activity);

        alarmList.setAdapter(adapter);
        setData();
    }

    public void setData() throws Exception {
        List<Seasons> oldData = adapter.getObjs();
        if (oldData == null) {
            oldData = new ArrayList<>();
        } else {
            oldData.clear();
        }
        List<Seasons> dataNew = model.getAll();
        oldData.addAll(dataNew);
        adapter.notifyDataSetChanged();
    }
}
