package vnapnic.project.alarmschool.views.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import vnapnic.project.alarmschool.common.Logger;
import vnapnic.project.alarmschool.controller.fragments.UpdateableFragment;
import vnapnic.project.alarmschool.views.fragment.AlarmFragment;
import vnapnic.project.alarmschool.views.fragment.ClockFragment;

/**
 * Created by hainam1421 on 1/16/2016.
 */
public class FragmentAdapter extends FragmentPagerAdapter {
    List<Fragment> fragmentList;

    public FragmentAdapter(FragmentManager fm) {
        super(fm);
        fragmentList = new ArrayList<>();
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getItemPosition(Object object) {
        if (object instanceof UpdateableFragment) {
            try {
                ((UpdateableFragment) object).update();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return super.getItemPosition(object);
    }

    public List<Fragment> getDate() {
        return fragmentList;
    }

    public void setData() {

        if (fragmentList != null) {
            fragmentList = new ArrayList<>();
        } else {
            fragmentList.clear();
        }

        List<Fragment> newData = new ArrayList<>();
        newData.add(new ClockFragment());
        newData.add(new AlarmFragment());

        fragmentList.addAll(newData);
        this.notifyDataSetChanged();

        Logger.d("iiiiiiiiiiiiiiiiii", fragmentList.size() + " -------- ");
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }
}
