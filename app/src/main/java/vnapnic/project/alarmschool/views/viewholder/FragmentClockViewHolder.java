package vnapnic.project.alarmschool.views.viewholder;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AnalogClock;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextClock;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import vnapnic.project.alarmschool.R;
import vnapnic.project.alarmschool.base.viewholder.BaseFragmentViewHolder;
import vnapnic.project.alarmschool.common.Instance;
import vnapnic.project.alarmschool.common.Logger;
import vnapnic.project.alarmschool.controller.fragments.IFragmentClock;
import vnapnic.project.alarmschool.model.SeasonsModel;
import vnapnic.project.alarmschool.model.entities.Alarm;
import vnapnic.project.alarmschool.model.entities.Seasons;
import vnapnic.project.alarmschool.model.entities.Setting;
import vnapnic.project.alarmschool.views.adapter.ClockListAdapter;
import vnapnic.project.alarmschool.views.adapter.SettingAdapter;
import vnapnic.project.alarmschool.views.custom.DigitalClock;

/**
 * Created by hainam1421 on 1/16/2016.
 */
public class FragmentClockViewHolder extends BaseFragmentViewHolder {
    private final String TAG = FragmentClockViewHolder.class.getName();

    private IFragmentClock iFragmentClock;
    public DigitalClock textClock;
    public AnalogClock analogClock;
    //    private ExpandableListView clockList;
    private ClockListAdapter adapter;
    private SeasonsModel model;


    public FragmentClockViewHolder(Activity activity, View root, IFragmentClock iFragmentClock) throws Exception {
        super(activity, root, iFragmentClock);

        this.iFragmentClock = iFragmentClock;
        textClock = (DigitalClock) root.findViewById(R.id.fragment_clock_text_clock);
//        clockList = (ExpandableListView) root.findViewById(R.id.clockList);
        model = new SeasonsModel(activity);
        analogClock = (AnalogClock) root.findViewById(R.id.fragment_clock_analog_clock);
        initialize(activity);
    }

    @Override
    protected void initialize(final Context context) throws Exception {
//        clockList.setDividerHeight(2);
//        clockList.setGroupIndicator(null);
//        clockList.setClickable(true);

        adapter = new ClockListAdapter(context);
        adapter.setInflater(
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE),
                activity);
//        clockList.setAdapter(adapter);
//        textClock.setTimeZone("GMT+7");
//        textClock.setFormat24Hour(FORMAT_DATE);
//        setData();
    }

    public void setData() throws Exception {
        List<Seasons> oldData = adapter.getObjs();
        if (oldData == null) {
            oldData = new ArrayList<>();
        } else {
            oldData.clear();
        }
        List<Seasons> dataNew = model.getAll();
        oldData.addAll(dataNew);
        adapter.notifyDataSetChanged();
    }
}
