package vnapnic.project.alarmschool.views.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import vnapnic.project.alarmschool.R;
import vnapnic.project.alarmschool.common.Instance;
import vnapnic.project.alarmschool.common.Logger;
import vnapnic.project.alarmschool.controller.activities.IMainActivities;
import vnapnic.project.alarmschool.model.entities.Alarm;
import vnapnic.project.alarmschool.model.entities.Setting;
import vnapnic.project.alarmschool.views.dialogs.DialogSetting;

/**
 * Created by hainam1421 on 1/28/2016.
 */
public class SettingAdapter extends BaseAdapter implements Instance, IMainActivities {

    private final String TAG = SettingAdapter.class.getName();

    private List<Setting> data;
    private LayoutInflater inflater;
    private Context context;
    private Activity act;
    private IMainActivities iMainActivities;

    public SettingAdapter(Context context, IMainActivities iMainActivities) {
        data = new ArrayList<>();
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.iMainActivities = iMainActivities;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        try {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.setting_item, parent, false);
                holder.textView = (TextView) convertView.findViewById(R.id.settingName);
                holder.imageView = (ImageView) convertView.findViewById(R.id.settingIcon);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            Setting obj = data.get(position);
            holder.textView.setText(obj.title);
            holder.imageView.setImageResource(obj.icon);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Logger.d(TAG, "setOnClickListener " + position);
                    final Dialog dialogLogin = new Dialog(context);
                    View view = dialogLogin.getLayoutInflater().inflate(R.layout.dialog_login, null, false);
                    dialogLogin.setContentView(view);
                    Button btnLogin = (Button) view.findViewById(R.id.dialog_login_login);
                    Button btnOut = (Button) view.findViewById(R.id.dialog_login_cancel);
                    final EditText edtUer = (EditText) view.findViewById(R.id.dialog_login_user);
                    final EditText edtPass = (EditText) view.findViewById(R.id.dialog_login_pass);
                    btnOut.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogLogin.dismiss();
                        }
                    });
                    btnLogin.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (USER.equals(edtUer.getText().toString().trim().toLowerCase()) && PASS.equals(edtPass.getText().toString().trim().toLowerCase())) {
                                Dialog dialog;
                                switch (position) {
                                    case TYPE_SETTING_SEASON:
                                        dialog = new DialogSetting(context, TYPE_SETTING_SEASON, SettingAdapter.this);
                                        dialog.show();
                                        break;
                                    case TYPE_OVERLOOK:
                                        dialog = new DialogSetting(context, TYPE_OVERLOOK, SettingAdapter.this);
                                        dialog.show();
                                        break;
                                    case TYPE_ADD:
                                        dialog = new DialogSetting(context, TYPE_ADD, SettingAdapter.this);
                                        dialog.show();
                                        break;
                                }
                                dialogLogin.dismiss();
                            } else {
                                Snackbar.make(v, "Đăng nhập thất bại.!", Snackbar.LENGTH_SHORT).show();
                            }
                        }
                    });
                    dialogLogin.show();
                }
            });
            return convertView;
        } catch (Exception e) {
            e.printStackTrace();
            convertView = new View(context);
            return convertView;
        }
    }

    public List<Setting> getData() {
        return data;
    }

    public void setData(List<Setting> data) {
        this.data = data;
    }

    @Override
    public void onPickMusic(Alarm alarm, DialogSettingAdapter adapter) throws Exception {
        iMainActivities.onPickMusic(alarm, adapter);
    }

    private class ViewHolder {
        private TextView textView;
        private ImageView imageView;
    }
}
