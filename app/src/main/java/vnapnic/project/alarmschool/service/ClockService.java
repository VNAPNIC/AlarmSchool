package vnapnic.project.alarmschool.service;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.RemoteViews;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import vnapnic.project.alarmschool.Constants;
import vnapnic.project.alarmschool.R;
import vnapnic.project.alarmschool.common.Instance;
import vnapnic.project.alarmschool.widget.AlarmWidget;

public class ClockService extends Service implements Instance {
    //    protected AppWidgetProvider receiver;
//
//    public ClockService() {
//        Constants.log(getClass().getSimpleName() + "::newInstance");
//    }
//
//    private Timer clockTimer;
//    private final TimerTask clockTask = new TimerTask() {
//        @Override
//        public void run() {
//            mHandler.post(mUpdateResults);
//        }
//    };
//
//    final Handler mHandler = new Handler();
//    final Runnable mUpdateResults = new Runnable() {
//        public void run() {
//            update();
//        }
//    };

//    private void update() {
//        String datetime = Datetime();
//
//    }

//    public String Datetime() {
//        Calendar c = Calendar.getInstance();
////        System.out.println("Current time => " + c.getTime());
//        SimpleDateFormat df = new SimpleDateFormat(FORMAT_DATE);
//        return df.format(c.getTime());
//    }

//    private void init() {
//        clockTimer = new Timer();
//        clockTimer.scheduleAtFixedRate(clockTask, 0, 1000);
//    }

    @Override
    public IBinder onBind(Intent intent) {
        Constants.log(getClass().getSimpleName() + "::onBind");
        return null;
    }

    @Override
    public void onStart(Intent intent, int startId) {
        Constants.log(getClass().getSimpleName() + "::onStart");
        super.onStart(intent, startId);
//        init();
    }

    @Override
    public void onCreate() {

        Constants.log(getClass().getSimpleName() + "::onCreate");
//        init();
        super.onCreate();
//        init();
//        createReceiver();
//        registerReceiver( receiver, new IntentFilter( Intent.ACTION_TIME_TICK ) );
    }
//
//    protected void createReceiver()
//    {
//        receiver = new AlarmWidget();
//        Constants.log( getClass().getSimpleName() + "::createReceiver: " + receiver );
//    }

    @Override
    public void onDestroy() {
        Constants.log(getClass().getSimpleName() + "::onDestroy");
//        unregisterReceiver();
        super.onDestroy();
    }

//    protected void unregisterReceiver()
//    {
//        Constants.log( getClass().getSimpleName() + "::unregisterReceiver: " + receiver );
//        unregisterReceiver( receiver );
//    }
}
