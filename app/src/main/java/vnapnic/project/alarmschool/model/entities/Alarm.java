package vnapnic.project.alarmschool.model.entities;

import java.util.List;

/**
 * Created by hainam1421 on 1/20/2016.
 */
public class Alarm extends BaseAlarmEntities {
    public int alarmID;
    public String title;
    public String alarmTimer;
    public String alarmStop;
    public String music;
    public int isRun;
    public int seasonsID;
    public List<RepeatWeekdays> objs;
}
