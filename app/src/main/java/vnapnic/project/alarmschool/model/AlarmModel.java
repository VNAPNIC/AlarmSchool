package vnapnic.project.alarmschool.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import vnapnic.project.alarmschool.base.model.BaseDataModel;
import vnapnic.project.alarmschool.common.Logger;
import vnapnic.project.alarmschool.model.entities.Alarm;
import vnapnic.project.alarmschool.model.entities.BaseAlarmEntities;
import vnapnic.project.alarmschool.model.entities.RepeatWeekdays;

/**
 * Created by hainam1421 on 1/20/2016.
 */
public class AlarmModel extends BaseDataModel {

    private final String TAG = AlarmModel.class.getName();

    public AlarmModel(Context context) {
        super(context);
    }


    public List<Alarm> getAll() {
        try {
            List<Alarm> objs = new ArrayList<>();
            openDataBase();
            Cursor c = data.query(ALARM_DB_NAME, null, null, null, null, null, null);

            int id = c.getColumnIndex(ALARM_ID);
            int title = c.getColumnIndex(ALARM_TITLE);
            int alarmTimer = c.getColumnIndex(ALARM_TIMER);
            int alarmStop = c.getColumnIndex(ALARM_TIME_STOP);
            int music = c.getColumnIndex(ALARM_MUSIC);
            int isRun = c.getColumnIndex(ALARM_RUN);
            int seasonsID = c.getColumnIndex(ALARM_SEASON_ID);
            if (c.moveToFirst()) {
                do {
                    Alarm obj = new Alarm();
                    obj.alarmID = c.getInt(id);
                    obj.title = c.getString(title);
                    obj.alarmTimer = c.getString(alarmTimer);
                    obj.alarmStop = c.getString(alarmStop);
                    obj.music = c.getString(music);
                    obj.isRun = c.getInt(isRun);
                    obj.seasonsID = c.getInt(seasonsID);
                    obj.objs = getRepwatById(c.getInt(id));
//                    Logger.d(TAG,getRepwatById(c.getInt(id)).get(0).repeatWeekday.toString());
                    objs.add(obj);
                } while (c.moveToNext());
            }
            c.close();
            data.close();
            return objs;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public Cursor setCursor(int id) throws Exception {
        String[] args = new String[]{id + ""};
        String query = "SELECT * FROM " + REPEAT_DB_NAME + " WHERE " + REPEAT_ALARM_ID + "=?";
        Cursor c = data.rawQuery(query, args);
        return c;
    }

    public boolean update(Alarm obj) {
        try {
            openDataBase();
            ContentValues values = setContentValues(obj);
            return data.update(ALARM_DB_NAME, values, ALARM_ID + "=?", new String[]{obj.alarmID + ""}) > 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean add(Alarm obj) {
        try {
            openDataBase();
            ContentValues values = setContentValues(obj);
            return data.insert(ALARM_DB_NAME, null, values) > 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public int getTopID() {
        try {
            int id = 0;
            try {
                Cursor c = data.rawQuery("select " + ALARM_ID + " from " + ALARM_DB_NAME + " order by " + ALARM_ID + " DESC limit 1", null);
                int idIndex = c.getColumnIndex(ALARM_ID);
                if (c != null && c.getCount() > 0) {
                    if (c.moveToFirst()) {
                        id = c.getInt(idIndex);
                    }
                }

                c.close();
            } catch (Exception sqle) {
            }
            return id;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public boolean remove(int id) {
        try {
            openDataBase();
            return data.delete(ALARM_DB_NAME, ALARM_ID + "=?", new String[]{id + ""}) > 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public List<RepeatWeekdays> getRepwatById(int id) {
        try {
            List<RepeatWeekdays> objs = new ArrayList<>();
            Cursor c = setCursor(id);

            int repeatWeekdayID = c.getColumnIndex(REPEAT_ID);
            int repeatWeekday = c.getColumnIndex(REPEAT_WEEK_DAY);
            int alarmID = c.getColumnIndex(REPEAT_ALARM_ID);

            if (c.moveToFirst()) {
                do {
                    RepeatWeekdays obj = new RepeatWeekdays();
                    obj.repeatWeekdayID = c.getInt(repeatWeekdayID);
                    obj.repeatWeekday = c.getString(repeatWeekday);
                    obj.alarmID = c.getInt(alarmID);
                    objs.add(obj);
                } while (c.moveToNext());
            }

            c.close();
            return objs;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean addRepeat(RepeatWeekdays obj) {
        try {
            openDataBase();
            obj.alarmID = getTopID();
            Logger.d(TAG, getTopID() + " pppppppppppppppppppppppp");
            ContentValues values = setContentValues(obj);
            return data.insert(REPEAT_DB_NAME, null, values) > 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean updateRepeat(RepeatWeekdays obj) {
        try {
            openDataBase();
            ContentValues values = setContentValues(obj);
            return data.update(REPEAT_DB_NAME, values, REPEAT_ID + "=?", new String[]{obj.repeatWeekdayID + ""}) > 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public ContentValues setContentValues(RepeatWeekdays obj) {
        ContentValues values = new ContentValues();
        values.put(REPEAT_WEEK_DAY, obj.repeatWeekday);
        values.put(REPEAT_ALARM_ID, obj.alarmID);
        return values;
    }

    public ContentValues setContentValues(Alarm obj) {
        ContentValues values = new ContentValues();
        values.put(ALARM_TITLE, obj.title);
        values.put(ALARM_TIMER, obj.alarmTimer);
        values.put(ALARM_TIME_STOP, obj.alarmStop);
        values.put(ALARM_MUSIC, obj.music);
        values.put(ALARM_RUN, obj.isRun);
        values.put(ALARM_SEASON_ID, obj.seasonsID);
        return values;
    }
}
