package vnapnic.project.alarmschool.model.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hainam1421 on 1/20/2016.
 */
public class Seasons extends BaseAlarmEntities {
    public int seasonsID;
    public String title;
    public int isRun;
    public String months;
    public List<Alarm> objs =  new ArrayList();
}
