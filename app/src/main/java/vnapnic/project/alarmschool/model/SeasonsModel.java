package vnapnic.project.alarmschool.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import vnapnic.project.alarmschool.base.model.BaseDataModel;
import vnapnic.project.alarmschool.common.Logger;
import vnapnic.project.alarmschool.model.entities.Alarm;
import vnapnic.project.alarmschool.model.entities.RepeatWeekdays;
import vnapnic.project.alarmschool.model.entities.Seasons;

/**
 * Created by hainam1421 on 1/20/2016.
 */
public class SeasonsModel extends BaseDataModel {
    private final String TAG = SeasonsModel.class.getName();

    public SeasonsModel(Context context) {
        super(context);
    }

    public List<Seasons> getAll() {
        try {
            List<Seasons> objs = new ArrayList<>();
            openDataBase();
            Cursor c = data.query(SEASON_DB_NAME, null, null, null, null, null, null);
            int id = c.getColumnIndex(SEASON_ID);
            int title = c.getColumnIndex(SEASON_TITLE);
            int isRun = c.getColumnIndex(SEASON_RUN);
            int months = c.getColumnIndex(SEASON_MONTH);
            if (c.moveToFirst()) {
                do {
                    Seasons obj = new Seasons();
                    obj.seasonsID = c.getInt(id);
                    obj.title = c.getString(title);
                    obj.isRun = c.getInt(isRun);
                    obj.months = c.getString(months);
                    obj.objs = getByIdAlarm(c.getInt(id));
                    objs.add(obj);
                } while (c.moveToNext());
            }
            c.close();
            data.close();
            return objs;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public List<Alarm> getByIdAlarm(int id) {
        try {
            List<Alarm> objs = new ArrayList<>();
            Logger.d(TAG, "Season id: " + id);
            Cursor c = setCursorAlarm(id);

            int alarmID = c.getColumnIndex(ALARM_ID);
            int title = c.getColumnIndex(ALARM_TITLE);
            int alarmTimer = c.getColumnIndex(ALARM_TIMER);
            int alarmStop = c.getColumnIndex(ALARM_TIME_STOP);
            int music = c.getColumnIndex(ALARM_MUSIC);
            int isRun = c.getColumnIndex(ALARM_RUN);
            int seasonsID = c.getColumnIndex(ALARM_SEASON_ID);
            if (c.moveToFirst()) {
                do {
                    Alarm obj = new Alarm();
                    obj.alarmID = c.getInt(alarmID);
                    obj.title = c.getString(title);
                    obj.alarmTimer = c.getString(alarmTimer);
                    obj.alarmStop = c.getString(alarmStop);
                    obj.music = c.getString(music);
                    obj.isRun = c.getInt(isRun);
                    obj.seasonsID = c.getInt(seasonsID);
                    obj.objs = getRepwatById(c.getInt(alarmID));
                    objs.add(obj);
                } while (c.moveToNext());
            }

            c.close();
            return objs;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public List<RepeatWeekdays> getRepwatById(int id) {
        try {
            List<RepeatWeekdays> objs = new ArrayList<>();
            Cursor c = setCursorWeekday(id);

            int repeatWeekdayID = c.getColumnIndex(REPEAT_ID);
            int repeatWeekday = c.getColumnIndex(REPEAT_WEEK_DAY);
            int alarmID = c.getColumnIndex(REPEAT_ALARM_ID);

            if (c.moveToFirst()) {
                do {
                    RepeatWeekdays obj = new RepeatWeekdays();
                    obj.repeatWeekdayID = c.getInt(repeatWeekdayID);
                    obj.repeatWeekday = c.getString(repeatWeekday);
                    obj.alarmID = c.getInt(alarmID);
                    objs.add(obj);
                } while (c.moveToNext());
            }

            c.close();
            return objs;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Cursor setCursorAlarm(int id) throws Exception {
        String[] args = new String[]{id + ""};
        String query = "SELECT * FROM " + ALARM_DB_NAME + " WHERE " + ALARM_SEASON_ID + "=?";
        Cursor c = data.rawQuery(query, args);
        return c;
    }

    public Cursor setCursorWeekday(int id) throws Exception {
        String[] args = new String[]{id + ""};
        String query = "SELECT * FROM " + REPEAT_DB_NAME + " WHERE " + REPEAT_ALARM_ID + "=?";
        Cursor c = data.rawQuery(query, args);
        return c;
    }

    private ContentValues setContentValues(Seasons entity) throws Exception {
        ContentValues values = new ContentValues();
        values.put(SEASON_TITLE, entity.title);
        values.put(SEASON_RUN, entity.isRun);
        values.put(SEASON_MONTH, entity.months);
        return values;
    }

    public boolean update(Seasons entity) {
        try {
            openDataBase();
            ContentValues values = setContentValues(entity);
            return data.update(SEASON_DB_NAME, values, SEASON_ID + "=?", new String[]{String.valueOf(entity.seasonsID)}) > 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
