package vnapnic.project.alarmschool.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import vnapnic.project.alarmschool.base.model.BaseDataModel;
import vnapnic.project.alarmschool.model.entities.OverLook;

/**
 * Created by hainam1421 on 1/31/2016.
 */
public class OverLookModel extends BaseDataModel {
    private final String TAG = SeasonsModel.class.getName();

    public OverLookModel(Context context) {
        super(context);
    }

    public List<OverLook> getAll() {
        try {
            List<OverLook> objs = new ArrayList<>();
            openDataBase();
            Cursor c = data.query(OVERLOOK_DB_NAME, null, null, null, null, null, null);
            int id = c.getColumnIndex(OVERLOOK_ID);
            int title = c.getColumnIndex(VERLOOK_TITLE);
            int overlook = c.getColumnIndex(VERLOOK_TEXT);
            if (c.moveToFirst()) {
                do {
                    OverLook obj = new OverLook();
                    obj.overlookDayID = c.getInt(id);
                    obj.title = c.getString(title);
                    obj.overlook = c.getString(overlook);
                    objs.add(obj);
                } while (c.moveToNext());
            }
            c.close();
            data.close();
            return objs;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public boolean update(OverLook entity) {
        try {
            openDataBase();
            ContentValues values = setContentValues(entity);
            return data.update(OVERLOOK_DB_NAME, values, OVERLOOK_ID + "=?", new String[]{String.valueOf(entity.overlookDayID)}) > 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private ContentValues setContentValues(OverLook entity) throws Exception {
        ContentValues values = new ContentValues();
        values.put(VERLOOK_TITLE, entity.title);
        values.put(VERLOOK_TEXT, entity.overlook);
        return values;
    }

    public boolean remove(int id) {
        try {
            openDataBase();
            return data.delete(OVERLOOK_DB_NAME, OVERLOOK_ID + "=?", new String[]{id + ""}) > 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean add(OverLook entity) {
        try {
            openDataBase();
            ContentValues values = setContentValues(entity);
            return data.insert(OVERLOOK_DB_NAME, null, values) > 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
