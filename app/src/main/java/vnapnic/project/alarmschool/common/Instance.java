package vnapnic.project.alarmschool.common;

/**
 * Created by hainam1421 on 1/15/2016.
 */
public interface Instance {
    String CACHE_NAME = "alarmschool_cache";
    String CACHE_USER = "user_cache";
    String CACHE_PASS = "pass_cache";
    String FORMAT_DATE = "HH:mm:ss\ndd-MM";

    String USER = "kientrantbd@gmail.com";
    String PASS = "123456";

    int version = 1;
    String DB_NAME = "AlarmSchool.sqlite";
    String DB_PATH = "/data/data/vnapnic.project.alarmschool/databases/";

    //SeasonsModel
    String SEASON_DB_NAME = "seasons";
    String SEASON_ID = "seasonsID";
    String SEASON_TITLE = "title";
    String SEASON_RUN = "isRun";
    String SEASON_MONTH = "monthDefine";

    //OverLookModel
    String OVERLOOK_DB_NAME = "overlook";
    String OVERLOOK_ID = "overlookDayID";
    String VERLOOK_TITLE = "title";
    String VERLOOK_TEXT = "overlook";

    //AlarmModel
    String ALARM_DB_NAME = "alarm";
    String ALARM_ID = "alarmID";
    String ALARM_TITLE = "title";
    String ALARM_TIMER = "alarmTimer";
    String ALARM_TIME_STOP = "alarmStop";
    String ALARM_MUSIC = "music";
    String ALARM_RUN = "isRun";
    String ALARM_SEASON_ID = "seasonsID";

    //repeatWeekdays
    String REPEAT_DB_NAME = "repeatWeekdays";
    String REPEAT_ID = "repeatWeekdayID";
    String REPEAT_WEEK_DAY = "repeatWeekday";
    String REPEAT_ALARM_ID = "alarmID";

    //TYPE Setting
    int TYPE_SETTING_SEASON = 0;
    int TYPE_OVERLOOK = 1;
    int TYPE_ADD = 2;
    int TYPE_NULL = -1;

    //Activity Result
    String RETURN_DIMISS_DIALOG = "update";

    //TypeDatePicker
    int TYPE_START = 1;
    int TYPE_STOP = 2;

    //QCODE
    int REQ_CODE_PICK_SOUNDFILE = 100;
}
