package vnapnic.project.alarmschool.common;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import vnapnic.project.alarmschool.views.dialogs.OverlayService;

/**
 * Created by hainam1421 on 2/1/2016.
 */
public class media {
    private static MediaPlayer mediaPlayer;

    public static void run(Context context, String url) throws Exception {
        if (mediaPlayer == null) {
            mediaPlayer = new MediaPlayer();
        }
        if (!mediaPlayer.isPlaying()) {
            mediaPlayer.reset();
            mediaPlayer.setDataSource(context, Uri.parse(url));
            mediaPlayer.prepare();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.start();
        }
    }

    public static void stop(Activity activity) throws Exception {
        Logger.d("mediaPlayer","Stopppppppppp");
        if (mediaPlayer == null) {
            mediaPlayer = new MediaPlayer();
        }
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            mediaPlayer.seekTo(0);
        }
        Intent intent = new Intent(activity, OverlayService.class);
        activity.stopService(intent);
    }

    public static double getDuration() {
        return (double) mediaPlayer.getDuration();
    }
}
