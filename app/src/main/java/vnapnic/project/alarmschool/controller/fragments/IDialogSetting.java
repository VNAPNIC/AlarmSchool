package vnapnic.project.alarmschool.controller.fragments;

import vnapnic.project.alarmschool.model.entities.Alarm;
import vnapnic.project.alarmschool.views.adapter.DialogSettingAdapter;

/**
 * Created by hainam1421 on 1/31/2016.
 */
public interface IDialogSetting {
    public void onRemove(int id) throws Exception;
    public void onPickMusic(Alarm alarm,DialogSettingAdapter adapter) throws Exception;
}
