package vnapnic.project.alarmschool.controller.activities;

import vnapnic.project.alarmschool.base.controller.BaseInterface;
import vnapnic.project.alarmschool.model.entities.Alarm;
import vnapnic.project.alarmschool.views.adapter.DialogSettingAdapter;

/**
 * Created by hainam1421 on 1/15/2016.
 */
public interface IMainActivities extends BaseInterface {
    void onPickMusic(Alarm alarm, DialogSettingAdapter adapter) throws Exception;
}
