package vnapnic.project.alarmschool.controller.fragments;

/**
 * Created by hainam1421 on 1/29/2016.
 */
public interface UpdateableFragment {
    void update() throws Exception;
}
