package vnapnic.project.alarmschool.controller;

import android.view.View;

import java.util.Calendar;

import vnapnic.project.alarmschool.model.entities.BaseAlarmEntities;
import vnapnic.project.alarmschool.views.adapter.DialogSettingAdapter;

/**
 * Created by hainam1421 on 1/31/2016.
 */
public interface IDialogTimePiker {
    void onTimePicker(String date,DialogSettingAdapter.ViewHolder holder,View view,BaseAlarmEntities obj,int type) throws Exception;
}
