package vnapnic.project.alarmschool.widget;

import java.util.Date;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

import vnapnic.project.alarmschool.Constants;
import vnapnic.project.alarmschool.R;
import vnapnic.project.alarmschool.activities.CleanClockWidgetConfigure41;
import vnapnic.project.alarmschool.service.ClockService41;

/**
 * Created by hainam1421 on 2/25/2016.
 */
public class AlarmWidget41 extends AlarmWidget
{
    protected void startClockService( Context context )
    {
        context.startService( new Intent( ClockService41.class.getName() ) );
    }

    protected void stopClockService( Context context )
    {
        context.stopService( new Intent( ClockService41.class.getName() ) );
    }

    @Override
    protected void handleIntent( Context context, Intent intent )
    {
        Constants.log(getClass().getSimpleName() + "::handleIntent");

        if( intent.getAction().equals( Intent.ACTION_TIME_TICK ) )
        {
            AppWidgetManager manager = AppWidgetManager.getInstance( context );
            int appWidgetIds[] = manager.getAppWidgetIds( new ComponentName( context, AlarmWidget41.class.getName() ) );
            onUpdate( context, manager, appWidgetIds );
        }
    }

    @Override
    public void onUpdate( Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds )
    {
        Constants.log( getClass().getSimpleName() + "::onUpdate. " + appWidgetIds.length );

        final int N = appWidgetIds.length;
        for( int i = 0; i < N; i++ )
        {
            int appWidgetId = appWidgetIds[i];
            Constants.log( "appWidgetId: " + appWidgetId );

            boolean use24 = CleanClockWidgetConfigure41.loadPref(context, appWidgetId, Constants.PREF_24HOUR);
            boolean useShadow = CleanClockWidgetConfigure41.loadPref( context, appWidgetId, Constants.PREF_SHADOW );
            updateAppWidget( context, appWidgetManager, appWidgetId, use24, useShadow );
        }
    }

    public static void updateAppWidget( Context context, AppWidgetManager appWidgetManager, int appWidgetId, boolean use24,
                                        boolean useShadow )
    {
        Date date = new Date();
        int layoutId = useShadow ? R.layout.widget41 : R.layout.widget41_noshadow;
        AlarmWidget.updateAppWidget( context, appWidgetManager, appWidgetId, use24, date, layoutId );
    }
}