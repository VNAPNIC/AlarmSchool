package vnapnic.project.alarmschool.activities;


import android.appwidget.AppWidgetManager;
import android.content.Context;

import vnapnic.project.alarmschool.Constants;
import vnapnic.project.alarmschool.widget.AlarmWidget41;

/**
 * Created by hainam1421 on 2/25/2016.
 */
public class CleanClockWidgetConfigure41  extends CleanClockWidgetConfigure
{
    protected static final String PREF_PREFIX_KEY = "prefix41_";

    @Override
    protected void updateAppWidget( final Context context, boolean use24, boolean useShadow, AppWidgetManager appWidgetManager )
    {
        Constants.log(getClass().getSimpleName() + "::updateAppWidget");
        AlarmWidget41.updateAppWidget(context, appWidgetManager, mAppWidgetId, use24, useShadow);
    }

    @Override
    protected void savePrefs( final Context context, boolean use24, boolean useShadow )
    {
        Constants.log( getClass().getSimpleName() + "::savePrefs" );
        savePref( context, mAppWidgetId, Constants.PREF_24HOUR, use24 );
        savePref( context, mAppWidgetId, Constants.PREF_SHADOW, useShadow  );
    }

    static void savePref( Context context, int appWidgetId, String name, Boolean value )
    {
        savePref( context, appWidgetId, PREF_PREFIX_KEY, name, value );
    }

    public static boolean loadPref(Context context, int appWidgetId, String name)
    {
        return loadPref( context, appWidgetId, PREF_PREFIX_KEY, name );
    }

}