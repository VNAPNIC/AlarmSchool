package vnapnic.project.alarmschool.activities;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import vnapnic.project.alarmschool.R;
import vnapnic.project.alarmschool.base.activities.BaseActivities;
import vnapnic.project.alarmschool.common.Instance;
import vnapnic.project.alarmschool.common.Logger;
import vnapnic.project.alarmschool.common.media;
import vnapnic.project.alarmschool.controller.activities.IMainActivities;
import vnapnic.project.alarmschool.model.AlarmModel;
import vnapnic.project.alarmschool.model.entities.Alarm;
import vnapnic.project.alarmschool.model.entities.BaseAlarmEntities;
import vnapnic.project.alarmschool.views.adapter.DialogSettingAdapter;
import vnapnic.project.alarmschool.views.viewholder.MainActivityViewHolder;

public class MainActivity extends BaseActivities implements IMainActivities, Instance {

    private final String TAG = MainActivity.class.getName();
    private MainActivityViewHolder holder;
    private View root;
    private DialogSettingAdapter adapter;
    private Alarm alarm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            root = getLayoutInflater().inflate(R.layout.activity_main, null, false);
            setContentView(root);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            holder = new MainActivityViewHolder(this, root, this);
        } catch (Exception e) {
            e.printStackTrace();
            root = new View(getApplication());
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        holder.drawerToggle = new ActionBarDrawerToggle(this, holder.drawerLayout,
                holder.toolbar,
                R.string.open_drawer,
                R.string.close_drawer) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
                syncState();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
                syncState();
                holder.actionBar.setDefaultDisplayHomeAsUpEnabled(true);
            }
        };

        holder.drawerLayout.post(new Runnable() {
            @Override
            public void run() {
                holder.drawerToggle.syncState();
            }
        });
        holder.drawerLayout.setDrawerListener(holder.drawerToggle);

        holder.drawerLayout.setDrawerLockMode(holder.drawerLayout.LOCK_MODE_LOCKED_CLOSED);
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(MainActivity.this,CleanClockWidgetConfigure.class);
//                MainActivity.this.startActivity(intent);
                try {
                    media.stop(MainActivity.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            if (holder.drawerLayout.isDrawerOpen(GravityCompat.START)) {
                holder.drawerLayout.closeDrawers();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected String getTag() {
        return TAG;
    }

    @Override
    public void onPickMusic(Alarm alarm, DialogSettingAdapter adapter) throws Exception {
        this.adapter = adapter;
        this.alarm = alarm;
        Intent intent = new
                Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent,
                REQ_CODE_PICK_SOUNDFILE);
    }

    public void onUpdateUI() throws Exception {
        Logger.d(TAG, "onUpdateUIiiiiiiiiiiiiiiiiiiiiiii");
        holder.setViewPage();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_CODE_PICK_SOUNDFILE && resultCode == RESULT_OK) {
            if ((data != null) && (data.getData() != null)) {
                Uri uri = Uri.parse(data.getDataString());
                String path = getRealPathFromURI(getApplicationContext(), uri);
                Logger.d(TAG, "url mSelectedAlarm : " + path);
                alarm.music = path;
                AlarmModel model = new AlarmModel(getApplication().getApplicationContext());
                if (model.update(alarm)) {
                    List<BaseAlarmEntities> oldData = adapter.getData();
                    if (oldData == null) {
                        oldData = new ArrayList<>();
                    } else {
                        oldData.clear();
                    }
                    AlarmModel alarmModel = new AlarmModel(getApplication());
                    List<Alarm> newDataAlarmModel = alarmModel.getAll();
                    for (int i = 0; i < newDataAlarmModel.size(); i++) {
                        newDataAlarmModel.get(i).viewType = TYPE_ADD;
                    }
                    oldData.addAll(newDataAlarmModel);
                    adapter.notifyDataSetChanged();
                }

            }
        }
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}