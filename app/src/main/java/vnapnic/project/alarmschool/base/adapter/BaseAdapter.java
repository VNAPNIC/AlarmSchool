package vnapnic.project.alarmschool.base.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.widget.BaseExpandableListAdapter;
import java.util.ArrayList;
import java.util.List;

import vnapnic.project.alarmschool.model.entities.Seasons;

/**
 * Created by hainam1421 on 1/20/2016.
 */
public abstract class BaseAdapter extends BaseExpandableListAdapter {
    private final String TAG =  BaseAdapter.class.getName();
    protected List<Seasons> objs;
    protected Context context;
    protected LayoutInflater inflater;
    protected Activity activity;

    public BaseAdapter(Context context) {
        objs = new ArrayList<>();
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public void setInflater(LayoutInflater inflater, Activity act) {
        this.inflater = inflater;
        activity = act;
    }
    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return objs.get(groupPosition).objs.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public int getGroupCount() {
        return objs.size();
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
    public void setData(List<Seasons> objs){
        this.objs = objs;
    }

    public List<Seasons> getObjs() {
        return objs;
    }
}
