package vnapnic.project.alarmschool.base.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

import vnapnic.project.alarmschool.common.Instance;
import vnapnic.project.alarmschool.model.DataAccessHelper;
import vnapnic.project.alarmschool.model.entities.BaseAlarmEntities;

/**
 * Created by hainam1421 on 1/20/2016.
 */
public abstract class BaseDataModel implements Instance {

    protected Context context;
    protected DataAccessHelper helper;
    protected SQLiteDatabase data;

    protected BaseDataModel(Context context) {
        try {
            this.context = context;
            helper = new DataAccessHelper(context, data);
            helper.createDataBase();
        } catch (Exception ioe) {
            ioe.printStackTrace();
        }
    }

    protected void openDataBase() throws Exception {
        // Open the database
        String myPath = DB_PATH + DB_NAME;
        data = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READWRITE);
    }
}
