package vnapnic.project.alarmschool.base.activities;

import android.support.v7.app.AppCompatActivity;

import vnapnic.project.alarmschool.base.controller.BaseInterface;

/**
 * Created by hainam1421 on 1/15/2016.
 */
public abstract class BaseActivities extends AppCompatActivity implements BaseInterface {
    protected String currentFragmentTag = "";

    public void setCurrentFragment(String currentFragmentTag) {
        this.currentFragmentTag = currentFragmentTag;
    }

    protected abstract String getTag();
}
