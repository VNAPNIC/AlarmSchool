package vnapnic.project.alarmschool.base.viewholder;

import android.content.Context;
import android.view.View;

import vnapnic.project.alarmschool.base.controller.BaseInterface;

/**
 * Created by hainam1421 on 1/15/2016.
 */
public abstract class BaseViewHolder {
    protected View root;
    protected Context context;
    protected BaseInterface mInterface;

    public BaseViewHolder(Context context, View root, BaseInterface mInterface) throws Exception {
        this.context = context;
        this.root = root;
        this.mInterface = mInterface;

    }

    protected abstract void initialize(Context context) throws Exception;
}
