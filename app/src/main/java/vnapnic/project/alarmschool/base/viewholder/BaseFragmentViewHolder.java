package vnapnic.project.alarmschool.base.viewholder;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import vnapnic.project.alarmschool.base.controller.BaseFragmentInterface;
import vnapnic.project.alarmschool.common.Instance;

/**
 * Created by hainam1421 on 1/16/2016.
 */
public abstract class BaseFragmentViewHolder implements Instance {

    protected View root;
    protected Activity activity;
    protected BaseFragmentInterface mInterface;

    public BaseFragmentViewHolder(Activity activity, View root, BaseFragmentInterface mInterface) throws Exception {
        this.activity = activity;
        this.root = root;
        this.mInterface = mInterface;
    }

    protected abstract void initialize(Context context) throws Exception;
}
