package vnapnic.project.alarmschool.base.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import vnapnic.project.alarmschool.R;
import vnapnic.project.alarmschool.base.activities.BaseActivities;
import vnapnic.project.alarmschool.base.controller.BaseFragmentInterface;
import vnapnic.project.alarmschool.common.Instance;

/**
 * Created by hainam1421 on 1/16/2016.
 */
public abstract class BaseFragments extends Fragment implements BaseFragmentInterface,Instance {
    protected View root;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try {
        } catch (Exception e) {
            e.printStackTrace();
            root = new View(getActivity());
        }
        return root;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        try {
            if (isVisibleToUser) {
                ((BaseActivities) getActivity()).setCurrentFragment(getTagId());
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    protected abstract String getTagId();
}
